organization hss
stakeholder zm [
	full name "Zach McNellis"
]
stakeholder dk [
	full name "David Kwietniewski"
]

stakeholder c [
	role "Customer"
]

stakeholder m [
	role "Manager"
]
stakeholder a [
	role "Architect"
]

stakeholder d [
	role "Developer"
]