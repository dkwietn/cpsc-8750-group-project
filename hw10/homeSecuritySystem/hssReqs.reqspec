system requirements hssReqs:"HSS" 
for home_security_system::home_security_system.impl
use constants hssConstants 
[
	compute HeatData
	compute ProximityData
	compute Latency
	compute NumConnections
	compute ResponseTime
	
	requirement siren_R1: "heat sensor data may trigger siren"
	[
		description this "At any time the security system controller receives information from the proximity sensors over the specified threshold, a siren will turn on and alert the home security network."
		rationale "Activate siren when the security system detects a potential intruder"
		value predicate HeatData < MinimumHeat
		mitigates "Invalid data sent by the heat sensor"
		issues "Need to interpret data as quickly as possible"
		see goal hssGoals.g1 hssGoals.g4
		category siren
		quality operability
		uncertainty[
			volatility 2
			impact 5
		]
	]
	
	requirement siren_R2: "proximity sensor data may trigger siren"
	[
		description this "At any time the security system controller receives information from the proximity sensors over the specified threshold, a siren will turn on and alert the home security network."
		rationale "Activate siren when the security system detects a potential intruder"
		value predicate ProximityData < MinimumProximity
		mitigates "Invalid data sent by the heat sensor"
		issues "Need to interpret data as quickly as possible"
		see goal hssGoals.g1 hssGoals.g4
		category siren
		quality operability 
		uncertainty[
			volatility 2
			impact 5
		]
	]	
	
	requirement protocol_R1: "requires http to communicate within max latency"
	[
		description this "Devices shall communicate over https with a latency of no more than 25ms"
		rationale "Cameras and Sensors can communicate with the Internet Interface"
		value predicate Latency < MaximumLatency
		mitigates "Insecure communication with Internet Interface"
		issues "Invalid data sent to controller"
		see goal hssGoals.g1 hssGoals.g2 hssGoals.g4
		category comm
		quality latency
		uncertainty[
			volatility 2
			impact 3
		]
	]
	
	requirement errors_R1: "the system handles invalid data"
	[
		description this "The system detects and reports invalid data sent by the heat sensor"
		rationale "Reduce incorrect detection of intruders"
		value predicate HeatData > 0
		mitigates "Invalid data sent by the heat sensor"
		issues "None"
		see goal hssGoals.g1 hssGoals.g2
		category invalid
		quality security
		uncertainty[
			volatility 3
			impact 3
		]
	]
	
	requirement errors_R2: "the system handles invalid data"
	[
		description this "The system detects and reports invalid data sent by the proximity sensor"
		rationale "Reduce incorrect detection of intruders"
		value predicate ProximityData > 0
		mitigates "Invalid data sent by the proximity sensor"
		issues "None"
		see goal hssGoals.g1 hssGoals.g2
		category invalid
		quality security
		uncertainty[
			volatility 3
			impact 3
		]
	]
	
	requirement errors_R3: "the system detects invalid connections"
	[
		description this "The system detects and reports invalid connections to the controller"
		rationale "Any external sensor or device trying to connect that is not registered with the security system will alert the user"
		value predicate NumConnections < MaxConnections
		mitigates "Unauthorized connection"
		issues "Override unauthorized connection capability"
		see goal hssGoals.g2
		category invalid
		quality security
		uncertainty[
			volatility 3
			impact 5
		]
	]
	
	requirement functionality_R2 : "The system is able to notify the customer if necessary."
	[
		description this "The system shall be able to notify the customer over https in less than 25 milliseconds in the event of an abnormal occurrence."
		rationale "This will improve the desirability of the product"
		value predicate ResponseTime < MaxResponseTime
		mitigates "Insecure communication with Internet Interface"
		issues "Invalid data sent to controller"
		see goal hssGoals.g1 hssGoals.g2 hssGoals.g4
		category comm
		quality latency
		uncertainty[
			volatility 2
			impact 3
		]
	]
	
	requirement functionality_R3 : "All system components interface with each other through a controller with."
	[
		description this "Communication to and from the controller takes a maximum of 1 second."
		rationale "This will improve the responsiveness of the product"
		value predicate CommunicationTime < MaxCommunicationTime
		mitigates "Problems affecting expandability"
		issues "I"
		see goal hssGoals.g1 hssGoals.g2 hssGoals.g4
		category comm
		quality latency operability
		uncertainty[
			volatility 2
			impact 4
		]
	]
	
	
]