package hssverify;

import org.osate.aadl2.instance.ComponentInstance;

@SuppressWarnings("all")
public class hssTestComp {
  public boolean comp(final ComponentInstance ci, final double a, final double b) {
    return (a > b);
  }
  public boolean compZero(final ComponentInstance ci, final double a) {
    return (a > 0.0);
  }
}
