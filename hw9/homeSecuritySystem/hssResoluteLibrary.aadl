package hssResoluteLibrary
public
	with hss_properties;
	
		annex Resolute{** 
				
				print_aadl(a:aadl)<=
				**a** 
				true	
				print_set(s:{aadl})<=
				**s**
				true
				
		Security_Features(self : component) <= **"Calculating total attack surface " **
			let featureSet : {feature} = features(self);
			let sumSurfaceC:real = sum({surfaceAreaC(t) for (t:featureSet)});
			let sumAccessC:real = sum({surfaceAccessC(w) for (w:featureSet)});
			Req5(sumSurfaceC/sumAccessC)
			
		Security_Connections(self : component) <= **"Calculating total attack surface " **
			let connectionSet : {connection} = connections(self);
			let sumSurfaceM:real = sum({surfaceAreaM(u) for (u:connectionSet)});
			let sumAccessM:real = sum({surfaceAccessM(v) for (v:connectionSet)});
			Req6(sumSurfaceM/sumAccessM)
			
		Security_Data(self : component) <= **"Calculating total attack surface " **
			let dataSet:{component}  = subcomponents(self);
			let sumSurfaceI:real = sum({surfaceAreaI(u) for (u:dataSet)});
			let sumAccessI:real = sum({surfaceAccessI(v) for (v:dataSet)});
		 	Req7(sumSurfaceI/(sumAccessI))
		 	
		Req5(total:real)<= **"DERC is " total**	true
		Req6(total:real)<= **"DERM is " total**	true
		Req7(total:real)<= **"DERI is " total**	true
	
		surfaceAreaC(self:feature):real=
		property(self,securityProperties::entryExitPointPrivileges,0.0)
			
		surfaceAreaM(self:connection):real=
		property(self,securityProperties::Channel_Protocol,0.0)
		
		surfaceAreaI(self:component):real=
		property(self,securityProperties::dataItemType,0.0)
		
		surfaceAccessC(self:feature):real=
		property(self,securityProperties::entryExitPointAccessRights,0.0)
		
		surfaceAccessM(self:connection):real=
		property(self,securityProperties::Channel_AccessRights,0.0)
		
		surfaceAccessI(self:component):real=
		property(self,securityProperties::dataItemAccessRights,0.0)
		
		SystemWideReq1() <= ** "All sensor, camera, internet interface, and controller threads have a period" ** 
 				forall (t: thread). HasPeriod(t)
 
 		HasPeriod(t : thread) <= ** "Thread " t " has a period" ** 
 				if is_of_type(t, sensor_system::sensor) then
 					has_property(t,Timing_Properties::Period)
 				else true
		
		SystemWideReq2(self : system) <= ** "All Systems have a bandwidth budget" ** 
			HasBandwidthBudget(self) and forall (c: subcomponents(self)). HasBandwidthBudget(c)
 
 		HasBandwidthBudget(t : component) <= ** "System " t " has a bandwidth budget" **
 				has_property(t, HSS_Properties::MaxBandwidth)
		
		SystemWideReq3(self : system) <= ** "All Systems have a power budget" ** 
			HasPowerBudget(self) and forall (c: subcomponents(self)). HasPowerBudget(c)
 
 		HasPowerBudget(t : component) <= ** "System " t " has a power budget" **
 				has_property(t, HSS_Properties::MaxBandwidth)
		
		SystemWideReq4() <= ** "All sensors have a power budget" ** 
 				forall (t: component).sensorHasPowerBudget(t)
 
 		sensorHasPowerBudget(t : component) <= ** "Sensor " t " has a power budget" ** 
 				 if is_of_type(t, sensor_system::sensor) then
 					has_property(t,HSS_Properties::MaxPower)
				else true
		
		SystemWideReq5() <= ** "All cameras have a power budget" ** 
 				forall (t: component).cameraHasPowerBudget(t)
 
 		cameraHasPowerBudget(t : component) <= ** "Camera " t " has a power budget" ** 
 				if is_of_type(t, camera_system::camera) then
 					has_property(t,HSS_Properties::MaxPower)
				else true
				
		SystemWideReq6(self : system) <= ** "All system have a communication priority" **
			forall (c: subcomponents(self)). hasPriority(c)
 				
 		hasPriority(t: component) <= ** "System " t " has a communication priority" **
 			 has_property(t, HSS_Properties::CommPriority)
 			
		SystemWideReq7(self : system) <= ** "Combined bandwidth budgets do not exceed system bandwidth budget" **
			sum({property(c, HSS_Properties::MaxBandwidth, 0gbs) for (c:subcomponents(self))}) <= property(self, HSS_Properties::MaxBandwidth, 99gbs)
 				
 		SystemWideReq8(self : system) <= ** "Combined power budgets do not exceed system power budget" **
			sum({property(c, HSS_Properties::MaxPower, 0MW) for (c:subcomponents(self))}) <= property(self, HSS_Properties::MaxPower, 99MW)
 				
 			
			**};
end hssResoluteLibrary;
