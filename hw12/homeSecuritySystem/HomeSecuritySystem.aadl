-- David Kwietniewski and Zach McNellis
-- Group Project

-- home_security_system.aadl

-- home security system

-- data types defined in hss_variables.aadl

-- The underlying architectural style follows client/server design
-- The Internet Interface module employs a model view controller to maintain up to date information
-- We used decomposition tactics to break the system apart into more modular components 
-- that interact with each other only as much as necessary
-- We embellished the architecture by defining error models and using AGREE 
-- for specifying variables and acceptable values

package home_security_system
	public
	with Base_Types,	
		 EMV2,	--error modeling properties
		 ErrorLibrary,
		 hss_variables,
		 SEI,
		 hssResoluteLibrary,
		 internet_interface,
		 controller,
		 camera_system,
		 lock,
		 siren,
		 sensor_system,
		 hss_properties;

annex EMV2
{**
	error types
		UnknownError : type; -- non-catastrophic error
		InternalError : type; --an internal error was detected
	end types;
	error behavior FailStop
		use types home_security_system;
		events fail: error event;
		states 
			working: initial state; 
			failed : state;
		transitions
			working -[fail]-> failed;
	end behavior;
**};

-- top-level component enclosing the curve speed warning system
system home_security_system
features
	internet_port : out data port Base_Types::Integer;
end home_security_system;

-- first-tier subcomponents and their connections
system implementation home_security_system.impl
subcomponents
	internet_interface: system internet_interface::internet_interface_hss.impl;
	controller: system controller::controller_hss.impl;
	sensor_system: system sensor_system::sensor_system_hss.impl;
	camera_system: system camera_system::camera_system_hss.impl;
	lock: system lock::lock_hss.impl;
	siren: system siren::siren_hss.impl;

connections
	ssh: port sensor_system.sensor_data -> controller.sensor_data;
	hii: port controller.controller_data -> internet_interface.controller_data;
	iih: port internet_interface.internet_data -> controller.internet_data;	
	csh: port camera_system.camera_data -> controller.camera_data;
	lud: port lock.user_data -> controller.user_data;
	luc: port lock.unlock_event -> camera_system.unlock_event;
	llc: port lock.lock_event -> camera_system.lock_event;
	lus: port lock.unlock_event -> sensor_system.unlock_event;
	lls: port lock.lock_event -> sensor_system.lock_event;
	ffd: port controller.alarmOn -> siren.alarmOn;
	ffe: port controller.alarmOff -> siren.alarmOff;
	iil: port internet_interface.lock_data -> lock.lock_data;
	iio: port internet_interface.outbound_data -> internet_port;
	eca: port controller.event_data -> camera_system.event_data;
	ese: port controller.event_data -> sensor_system.event_data;
properties
	hss_properties::MaxPower => 65 W;
	hss_properties::MaxBandwidth => 5 gbs;

	annex Resolute{**
		prove(print_aadl(this))
		prove(SystemWideReq1())
		prove(SystemWideReq2(this))
		prove(SystemWideReq3(this))
		prove(SystemWideReq4())
		prove(SystemWideReq5())
		prove(SystemWideReq6(this))
		prove(SystemWideReq7(this))
		prove(SystemWideReq8(this))
	**};
	
end home_security_system.impl;

end home_security_system;