package internet_interface
	public
	with Base_Types,	
		 EMV2,	--error modeling properties
		 ErrorLibrary,
			hss_variables,
			hss_properties;
			
system internet_interface_hss
features
	controller_data : in data port Base_Types::Float;
	internet_data : out data port Base_Types::Integer;
	outbound_data : out data port Base_Types::Integer;
	lock_data : out data port Base_Types::Integer;
	annex agree
	{**
		assume "Controller data range" : controller_data > -1.0;
		guarantee "Internet data and controller data range" : internet_data > 0;
		guarantee "Lock data range" : lock_data = 0 or lock_data = 1;
		guarantee "Available ports" : outbound_data = 22;
	**};
end internet_interface_hss;

system implementation internet_interface_hss.impl
	subcomponents
		model : memory model.impl;
		view : system view.impl;
		controller : system controller.impl;
		interface_controls : process interface_controls.impl;
	connections
		cue : port controller.update_event -> interface_controls.update_event;
		rue : port controller.restart_event -> interface_controls.restart_event;
		sue : port controller.save_event -> interface_controls.save_event;
		cuv : port controller.update -> view.update;
		vac : port view.user_action -> controller.user_action;
		cuo : port controller.update -> model.update;
		mnc : port model.notify -> controller.notify;
		cdc : port controller_data -> controller.controller_data;
		cob : port controller.outbound_data -> outbound_data;
		cld : port controller.lock_data -> lock_data;
		cii : port controller.internet_data -> internet_data;
	properties
			hss_properties::MaxPower => 10 W;
			hss_properties::MaxBandwidth => 1000 mbs;
			hss_properties::CommPriority => 1;
end internet_interface_hss.impl;

memory model
	features
		update : in data port Base_Types::Float;
		notify : out data port Base_Types::Float;
	annex agree
	{**
		assume "Update range" : update > 0.0 and update < 100.0;
		guarantee "Notify range" : notify > 0.0 and notify < 100.0;
 	**};
end model;

memory implementation model.impl
end model.impl;

system view
	features
		update : in data port Base_Types::Float;
		user_action : out data port Base_Types::Float;
	annex agree
	{**
		assume "Update range" : update > 0.0 and update < 100.0;
		guarantee "User action range" : user_action > 0.0 and user_action < 100.0;
 	**};
end view;

system implementation view.impl
	subcomponents
		present_view : process present_view.impl;
end view.impl;

process present_view
end present_view;

process implementation present_view.impl
	subcomponents
		set_view : thread set_view.impl;
end present_view.impl;

thread set_view
	properties
		Timing_Properties::Period => 1ms;
end set_view;

thread implementation set_view.impl
	subcomponents
		process_view : subprogram process_view;
	calls
	sequence : { action1 : subprogram process_view; } ;
end set_view.impl;

subprogram process_view
end process_view;

system controller
	features
		notify : in data port Base_Types::Float;
		user_action : in data port Base_Types::Float;
		update : out data port Base_Types::Float;
		update_event : out event port;
		restart_event : out event port;
		save_event : out event port;
		controller_data : in data port Base_Types::Float;
		internet_data : out data port Base_Types::Integer;
		lock_data : out data port Base_Types::Integer;
		outbound_data : out data port Base_Types::Integer;
	annex agree
	{**
		assume "Notify range" : notify > 0.0 and notify < 100.0;
		assume "User action range" : user_action > 0.0 and user_action < 100.0;
		assume "Controller data range" : controller_data > -1.0;
		guarantee "Update range" : update > 0.0 and update < 100.0;
		guarantee "Lock data range" : lock_data = 0 or lock_data = 1;
		guarantee "Available port" : outbound_data = 22;
 	**};
end controller;

system implementation controller.impl
	subcomponents
		interface : process interface.impl;
end controller.impl;

process interface
end interface;

process implementation interface.impl
	subcomponents
		interface_action : thread interface_action.impl;
		update : thread update.impl;
		restart : thread restart.impl;
end interface.impl;

thread interface_action
	properties
		Timing_Properties::Period => 2ms;
end interface_action;

thread implementation interface_action.impl
	subcomponents
		get_data : subprogram get_data;
		consolidate_data : subprogram consolidate_data;
		send_data : subprogram send_data;
	calls
	sequence : { action1 : subprogram get_data;
				 action2 : subprogram consolidate_data;
				 action3 : subprogram send_data; } ;
end interface_action.impl;

subprogram get_data	
end get_data;

subprogram consolidate_data
end consolidate_data;

subprogram send_data
end send_data;

process interface_controls
	features
		update_event : in event port;
		restart_event : in event port;
		save_event : in event port;
end interface_controls;

process implementation interface_controls.impl
	subcomponents
		update : thread update.impl;
		restart : thread restart.impl;
		save : thread save.impl;
	connections
		upor : port update_event -> update.update_event;
		rpor : port restart_event -> restart.restart_event;
		spor : port save_event -> save.save_event;
end interface_controls.impl;

thread restart
	features
		restart_event : in event port;
end restart;

thread implementation restart.impl
	subcomponents
		powerOn : subprogram powerOn.impl;
		powerOff : subprogram powerOff.impl;
end restart.impl;

thread update
	features
		update_event : in event port;
end update;

thread implementation update.impl
	subcomponents
		powerOn : subprogram powerOn.impl;
		powerOff : subprogram powerOff.impl;
end update.impl;

thread save
	features
		save_event : in event port;
end save;

thread implementation save.impl
	subcomponents
		powerOn : subprogram powerOn.impl;
		powerOff : subprogram powerOff.impl;
end save.impl;

subprogram powerOn
end powerOn;

subprogram implementation powerOn.impl
end powerOn.impl;

subprogram powerOff
end powerOff;

subprogram implementation powerOff.impl
end powerOff.impl;

end internet_interface;