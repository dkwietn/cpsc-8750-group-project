package camera_system
	public
	with Base_Types,	
		 EMV2,	--error modeling properties
		 ErrorLibrary,
			hss_variables,
			hss_properties;
			
system camera_system_hss
	features
		camera_data : out data port Base_Types::Float_64;
		event_data : in event data port Base_Types::Integer;
		lock_event : in event port;
		unlock_event : in event port;
	annex agree
	{**
		assume "Allowable event data types" : event_data = 0 or event_data = 1 or event_data = 2;		
		guarantee "Camera system output range" : camera_data > 0.0 and camera_data < 1.0e9;
	**};
end camera_system_hss;

system implementation camera_system_hss.impl
	subcomponents
		camera_front : system camera_standard.impl;
		camera_back : system camera_wide.impl;
		camera_proxy : process camera_proxy.impl;
		camera_controls : process camera_controls.impl;
	connections
		cfi : port camera_front.image_data -> camera_proxy.image_data in modes (active);
		cbi : port camera_back.image_data -> camera_proxy.image_data in modes (active);
		cfa : port camera_front.audio_data -> camera_proxy.audio_data in modes (active);
		cba : port camera_back.audio_data -> camera_proxy.audio_data in modes (active);
		cpd : port camera_proxy.camera_data -> camera_data in modes (active);
		edp : port event_data -> camera_proxy.event_data;
		sav : port camera_proxy.save_event -> camera_controls.save_event;
		upd : port camera_proxy.update_event -> camera_controls.update_event;
		res : port camera_proxy.restart_event -> camera_controls.restart_event;
	modes
		active: initial mode;
		inactive: mode;
		active-[unlock_event]->inactive;
		inactive-[lock_event]->active;
	properties
		hss_properties::MaxPower => 10 W;
		hss_properties::MaxBandwidth => 10 mbs;
		hss_properties::CommPriority => 4;
end camera_system_hss.impl;

system camera
	features
		image_data : out data port hss_variables::Two_Dim_Image;
		audio_data : out data port hss_variables::Audio_Data;
end camera;

system implementation camera.impl
	subcomponents
		battery : device battery.impl;
end camera.impl;

system camera_standard extends camera
	annex EMV2 
	{** 
		use types ErrorLibrary;
		use behavior home_security_system::FailStop;
		error propagations
			image_data: out propagation{OutOfRange,SubtleValueError};
			audio_data: out propagation{OutOfRange,SubtleValueError};
		flows
			f1 : error source image_data{OutOfRange,SubtleValueError};
			f2 : error source audio_data{OutOfRange,SubtleValueError};
		end propagations;
		properties
			EMV2::OccurrenceDistribution => [ProbabilityValue => 6.75e-5; Distribution => Fixed;] applies to image_data.OutOfRange;
			EMV2::OccurrenceDistribution => [ProbabilityValue => 2.50e-5; Distribution => Fixed;] applies to image_data.SubtleValueError;
			EMV2::OccurrenceDistribution => [ProbabilityValue => 3.00e-5; Distribution => Fixed;] applies to audio_data.OutOfRange;
			EMV2::OccurrenceDistribution => [ProbabilityValue => 7.50e-6; Distribution => Fixed;] applies to audio_data.SubtleValueError;
	**};
end camera_standard;

system implementation camera_standard.impl extends camera.impl
	properties
		hss_properties::MaxPower => 3 W;
end camera_standard.impl;

system camera_wide extends camera
	annex EMV2 
	{** 
		use types ErrorLibrary;
		use behavior home_security_system::FailStop;
		error propagations
			image_data: out propagation{OutOfRange,SubtleValueError};
			audio_data: out propagation{OutOfRange,SubtleValueError};
		flows
			f1 : error source image_data{OutOfRange,SubtleValueError};
			f2 : error source audio_data{OutOfRange,SubtleValueError};
		end propagations;
		properties
			EMV2::OccurrenceDistribution => [ProbabilityValue => 1.75e-5; Distribution => Fixed;] applies to image_data.OutOfRange;
			EMV2::OccurrenceDistribution => [ProbabilityValue => 3.50e-5; Distribution => Fixed;] applies to image_data.SubtleValueError;
			EMV2::OccurrenceDistribution => [ProbabilityValue => 4.00e-5; Distribution => Fixed;] applies to audio_data.OutOfRange;
			EMV2::OccurrenceDistribution => [ProbabilityValue => 3.50e-6; Distribution => Fixed;] applies to audio_data.SubtleValueError;
	**};
end camera_wide;

system implementation camera_wide.impl extends camera.impl
	properties
		hss_properties::MaxPower => 3 W;
end camera_wide.impl;

device battery
end battery;

device implementation battery.impl
end battery.impl;

process camera_proxy
	features
		image_data : in data port hss_variables::Two_Dim_Image;
		audio_data : in data port hss_variables::Audio_Data;
		camera_data : out data port Base_Types::Float_64;
		event_data : in event data port Base_types::Integer;
		update_event : out event port;
		restart_event : out event port;
		save_event : out event port;
	annex EMV2 
	{** 
		use types ErrorLibrary;
		use behavior home_security_system::FailStop;
		error propagations
			image_data: in propagation{OutOfRange,SubtleValueError};
			audio_data: in propagation{OutOfRange,SubtleValueError};
		flows
			f1 : error sink image_data{OutOfRange,SubtleValueError};
			f2 : error sink audio_data{OutOfRange,SubtleValueError};
		end propagations;
	**};
	annex agree
	{**
		guarantee "Camera system output range" : camera_data > 0.0 and camera_data < 1.0e9;
		assume "Allowable event data types" : event_data = 0 or event_data = 1 or event_data = 2;	
	**};
end camera_proxy;

process implementation camera_proxy.impl
	subcomponents
		actions : thread camera_proxy_actions.impl;
end camera_proxy.impl;

thread camera_proxy_actions
		properties
			Timing_Properties::Period => 2ms;
end camera_proxy_actions;
	
thread implementation camera_proxy_actions.impl
	subcomponents
		get_data : subprogram get_data;
		consolidate_data : subprogram consolidate_data;
		send_data : subprogram send_data;
	calls
	sequence : { action1 : subprogram get_data;
				 action2 : subprogram consolidate_data;
				 action3 : subprogram send_data; } ;
end camera_proxy_actions.impl;

subprogram get_data	
end get_data;

subprogram consolidate_data
end consolidate_data;

subprogram send_data
end send_data;

process camera_controls
	features
		update_event : in event port;
		restart_event : in event port;
		save_event : in event port;
end camera_controls;

process implementation camera_controls.impl
	subcomponents
		update : thread update.impl;
		restart : thread restart.impl;
		save : thread save.impl;
	connections
		upor : port update_event -> update.update_event;
		rpor : port restart_event -> restart.restart_event;
		spor : port save_event -> save.save_event;
end camera_controls.impl;

thread restart
	features
		restart_event : in event port;
end restart;

thread implementation restart.impl
	subcomponents
		powerOn : subprogram powerOn.impl;
		powerOff : subprogram powerOff.impl;
end restart.impl;

thread update
	features
		update_event : in event port;
end update;

thread implementation update.impl
	subcomponents
		powerOn : subprogram powerOn.impl;
		powerOff : subprogram powerOff.impl;
end update.impl;

thread save
	features
		save_event : in event port;
end save;

thread implementation save.impl
	subcomponents
		powerOn : subprogram powerOn.impl;
		powerOff : subprogram powerOff.impl;
end save.impl;

subprogram powerOn
end powerOn;

subprogram implementation powerOn.impl
end powerOn.impl;

subprogram powerOff
end powerOff;

subprogram implementation powerOff.impl
end powerOff.impl;

end camera_system;