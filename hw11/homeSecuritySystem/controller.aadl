package controller
	public
	with Base_Types,	
		 EMV2,	--error modeling properties
		 ErrorLibrary,
		 hss_properties,
		 hss_variables;
	
system controller_hss
features
	camera_data : in data port hss_variables::Camera_Data;
	sensor_data : in data port Base_Types::Float_64;
	internet_data : in data port;
	controller_data : out data port;
	user_data : in data port;
	alarmOn : out event port;
	alarmOff : out event port;

end controller_hss;

system implementation controller_hss.impl
	subcomponents
		logicSystem : process logicSystem.impl;
		dataSystem : process controller_proxy.impl;
	connections
		cdl : port camera_data -> logicSystem.camera_data;
		sdl : port sensor_data -> logicSystem.sensor_data;
		idl : port internet_data -> logicSystem.internet_data;
		lcc : port logicSystem.controller_data -> controller_data;
		laa : port logicSystem.alarmOn -> alarmOn;
		lab : port logicSystem.alarmOff -> alarmOff;	
	properties
		hss_properties::MaxPower => 20 W;
		hss_properties::MaxBandwidth => 1 gbs;
		hss_properties::CommPriority => 2;
end controller_hss.impl;

process logicSystem
	features
		camera_data : in data port hss_variables::Camera_Data;
		sensor_data : in data port Base_Types::Float_64;
		internet_data : in data port;
		controller_data : out data port;
		alarmOn : out event port;
		alarmOff : out event port;
end logicSystem;

process implementation logicSystem.impl
	subcomponents
		actions : thread logic_system_actions.impl;
end logicSystem.impl;

thread logic_system_actions
	properties
		Timing_Properties::Period => 2ms;
end logic_system_actions;

thread implementation logic_system_actions.impl
	calls
	sequence : { action1 : subprogram compute_action;};
end logic_system_actions.impl;

subprogram compute_action
end compute_action;

process controller_proxy
end controller_proxy;

process implementation controller_proxy.impl
	subcomponents
		actions : thread controller_proxy_actions.impl;
end controller_proxy.impl;

thread controller_proxy_actions
	properties
		Timing_Properties::Period => 2ms;
end controller_proxy_actions;

thread implementation controller_proxy_actions.impl
calls
sequence : { action1 : subprogram get_data;
			 action2 : subprogram consolidate_data;
			 action3 : subprogram send_data; } ;
end controller_proxy_actions.impl;

subprogram get_data	
end get_data;

subprogram consolidate_data
end consolidate_data;

subprogram send_data
end send_data;

end controller;