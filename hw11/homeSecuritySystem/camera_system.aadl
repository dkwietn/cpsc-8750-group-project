package camera_system
	public
	with Base_Types,	
		 EMV2,	--error modeling properties
		 ErrorLibrary,
			hss_variables,
			hss_properties;
			
system camera_system_hss
	features
		camera_data : out data port hss_variables::Camera_Data;
		lock_event : in event port;
		unlock_event : in event port;
end camera_system_hss;

system implementation camera_system_hss.impl
	subcomponents
		camera_front : system camera_standard.impl;
		camera_back : system camera_wide.impl;
		camera_proxy : process camera_proxy.impl;
		update : process update.impl;
		restart : process restart.impl;
		save : process save.impl;
	connections
		cfi : port camera_front.image_data -> camera_proxy.image_data in modes (active);
		cbi : port camera_back.image_data -> camera_proxy.image_data in modes (active);
		cfa : port camera_front.audio_data -> camera_proxy.audio_data in modes (active);
		cba : port camera_back.audio_data -> camera_proxy.audio_data in modes (active);
		cpd : port camera_proxy.camera_data -> camera_data in modes (active);
	modes
		active: initial mode;
		inactive: mode;
		active-[unlock_event]->inactive;
		inactive-[lock_event]->active;
	properties
		hss_properties::MaxPower => 10 W;
		hss_properties::MaxBandwidth => 10 mbs;
		hss_properties::CommPriority => 4;
end camera_system_hss.impl;

system camera
	features
		image_data : out data port hss_variables::Two_Dim_Image;
		audio_data : out data port hss_variables::Audio_Data;
end camera;

system implementation camera.impl
	subcomponents
		battery : device battery.impl;
end camera.impl;

system camera_standard extends camera
	annex EMV2 
	{** 
		use types ErrorLibrary;
		use behavior home_security_system::FailStop;
		error propagations
			image_data: out propagation{OutOfRange,SubtleValueError};
			audio_data: out propagation{OutOfRange,SubtleValueError};
		flows
			f1 : error source image_data{OutOfRange,SubtleValueError};
			f2 : error source audio_data{OutOfRange,SubtleValueError};
		end propagations;
		properties
			EMV2::OccurrenceDistribution => [ProbabilityValue => 6.75e-5; Distribution => Fixed;] applies to image_data.OutOfRange;
			EMV2::OccurrenceDistribution => [ProbabilityValue => 2.50e-5; Distribution => Fixed;] applies to image_data.SubtleValueError;
			EMV2::OccurrenceDistribution => [ProbabilityValue => 3.00e-5; Distribution => Fixed;] applies to audio_data.OutOfRange;
			EMV2::OccurrenceDistribution => [ProbabilityValue => 7.50e-6; Distribution => Fixed;] applies to audio_data.SubtleValueError;
	**};
end camera_standard;

system implementation camera_standard.impl extends camera.impl
	properties
		hss_properties::MaxPower => 3 W;
end camera_standard.impl;

system camera_wide extends camera
	annex EMV2 
	{** 
		use types ErrorLibrary;
		use behavior home_security_system::FailStop;
		error propagations
			image_data: out propagation{OutOfRange,SubtleValueError};
			audio_data: out propagation{OutOfRange,SubtleValueError};
		flows
			f1 : error source image_data{OutOfRange,SubtleValueError};
			f2 : error source audio_data{OutOfRange,SubtleValueError};
		end propagations;
		properties
			EMV2::OccurrenceDistribution => [ProbabilityValue => 1.75e-5; Distribution => Fixed;] applies to image_data.OutOfRange;
			EMV2::OccurrenceDistribution => [ProbabilityValue => 3.50e-5; Distribution => Fixed;] applies to image_data.SubtleValueError;
			EMV2::OccurrenceDistribution => [ProbabilityValue => 4.00e-5; Distribution => Fixed;] applies to audio_data.OutOfRange;
			EMV2::OccurrenceDistribution => [ProbabilityValue => 3.50e-6; Distribution => Fixed;] applies to audio_data.SubtleValueError;
	**};
end camera_wide;

system implementation camera_wide.impl extends camera.impl
	properties
		hss_properties::MaxPower => 3 W;
end camera_wide.impl;

device battery
end battery;

device implementation battery.impl
end battery.impl;

process camera_proxy
	features
		image_data : in data port hss_variables::Two_Dim_Image;
		audio_data : in data port hss_variables::Audio_Data;
		camera_data : out data port hss_variables::Camera_Data;
	annex EMV2 
	{** 
		use types ErrorLibrary;
		use behavior home_security_system::FailStop;
		error propagations
			image_data: in propagation{OutOfRange,SubtleValueError};
			audio_data: in propagation{OutOfRange,SubtleValueError};
		flows
			f1 : error sink image_data{OutOfRange,SubtleValueError};
			f2 : error sink audio_data{OutOfRange,SubtleValueError};
		end propagations;
	**};
end camera_proxy;

process implementation camera_proxy.impl
	subcomponents
		actions : thread camera_proxy_actions.impl;
end camera_proxy.impl;

thread camera_proxy_actions
		properties
			Timing_Properties::Period => 2ms;
end camera_proxy_actions;
	
thread implementation camera_proxy_actions.impl
calls
sequence : { action1 : subprogram get_data;
			 action2 : subprogram consolidate_data;
			 action3 : subprogram send_data; } ;
end camera_proxy_actions.impl;

subprogram get_data	
end get_data;

subprogram consolidate_data
end consolidate_data;

subprogram send_data
end send_data;

process restart
end restart;

process implementation restart.impl
end restart.impl;

process update
end update;

process implementation update.impl
end update.impl;

process save
end save;

process implementation save.impl
end save.impl;

end camera_system;