package internet_interface
	public
	with Base_Types,	
		 EMV2,	--error modeling properties
		 ErrorLibrary,
			hss_variables,
			hss_properties;
			
system internet_interface_hss
features
	controller_data : in data port;
	internet_data : out data port;
	outbound_data : out data port;
	lock_data : out data port;
end internet_interface_hss;

system implementation internet_interface_hss.impl
	subcomponents
		model : memory model.impl;
		view : system view.impl;
		controller : system controller.impl;
		update : process update.impl;
		restart : process restart.impl;
	properties
			hss_properties::MaxPower => 10 W;
			hss_properties::MaxBandwidth => 1000 mbs;
			hss_properties::CommPriority => 1;
end internet_interface_hss.impl;

memory model
end model;

memory implementation model.impl
end model.impl;

system view
end view;

system implementation view.impl
	subcomponents
		present_view : process present_view.impl;
end view.impl;

process present_view
end present_view;

process implementation present_view.impl
	subcomponents
		set_view : thread set_view.impl;
end present_view.impl;

thread set_view
	properties
		Timing_Properties::Period => 1ms;
end set_view;

thread implementation set_view.impl
calls
sequence : { action1 : subprogram process_view; } ;
end set_view.impl;

subprogram process_view
end process_view;

system controller
end controller;

system implementation controller.impl
	subcomponents
		interface : process interface.impl;
		update : process update.impl;
		restart : process restart;
end controller.impl;

process interface
end interface;

process implementation interface.impl
	subcomponents
		interface_action : thread interface_action.impl;
end interface.impl;

thread interface_action
	properties
		Timing_Properties::Period => 2ms;
end interface_action;

thread implementation interface_action.impl
calls
sequence : { action1 : subprogram get_data;
			 action2 : subprogram consolidate_data;
			 action3 : subprogram send_data; } ;
end interface_action.impl;

subprogram get_data	
end get_data;

subprogram consolidate_data
end consolidate_data;

subprogram send_data
end send_data;

process restart
end restart;

process implementation restart.impl
end restart.impl;

process update
end update;

process implementation update.impl
end update.impl;

end internet_interface;